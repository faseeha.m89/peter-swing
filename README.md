## Introduction

Peter-swing is a java swing look and feel. With advanced swing components. 

## Advanced swing components

https://github.com/mcheung63/peter-swing/wiki

## Compile

It is a maven project, so you can just clone the project and compile it by "mvn compile". If you want to package it as a jar, type in "mvn package" or "mvn install" to your local repository.

Author : Peter , mcheung63@hotmail.com

## Download

https://github.com/mcheung63/peter-swing/wiki/Releases

## Tutorial

https://github.com/mcheung63/peter-swing/wiki

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2015/08/peterswing-1.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2015/08/peterswing-2.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2015/08/peterswing-3.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2015/08/peterswing-4.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2015/08/peterswing-5.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2015/08/peterswing-6.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2015/08/peterswing-7.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2015/08/peterswing-8.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2015/08/peterswing-9.png)

![alt tag](http://peter.kingofcoders.com/wp-content/uploads/2015/08/peterswing-10.png)
